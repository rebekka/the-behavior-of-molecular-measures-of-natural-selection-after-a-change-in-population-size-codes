### weighted ω after Eyre-Walker 2002 ### 

# packages for integration
using HCubature

# other packages
using SpecialFunctions
using ProgressLogging
using DelimitedFiles

# pdf of the reflected Gamma-distribution
function reflected_gamma(γ, a, b)
    # γ             selection coefficient
    # a             shape parameter of gamma distribution
    # b             scale parameter of gamma distribution

    (-γ)^(a - 1) * exp(γ / b) / (b^a * gamma(a))
end

# definition of the fixation rate ω_{γ,κ} in equilibrium for fixed γ
function ω(γ, κ)
    # γ             selection coefficient
    # κ             population size factor

    if γ == 0
        ω = 1.0
    else
        γ2κ = 2 * γ * κ
        ω = γ2κ / (1 - exp(-γ2κ))
    end
end

# ω with DFE
function ω_infty(κ; θ = 1, a = 0.15, b = 2500 / 0.15)
    # κ             population size factor
    # θ             population scaled mutational input
    # a             shape parameter of gamma distribution
    # b             scale parameter of gamma distribution

    hquadrature(γ -> (reflected_gamma(γ, a, b) * ω(γ, κ)), -1000, -eps() * 10; rtol=1e-6)[1]
end

# weighted ω during nonequilibrium
function ω_noneq_EW(filename, κs, tstar, T, lt; kwargs...)
    # filename      name of the file to be saved
    # κs            vector of different values of κ
    # tstar         time of change in population size
    # T             number of time units
    # lt            length of time vector t

    # time points for t > t*
    ts = tstar .+ range(eps(); stop = T, length = lt)           

    # development after change in demography
    # matrix for saving results
    results = Matrix{Float64}(undef, lt, length(κs) + 1)        
    results[:, 1] = ts

    ProgressLogging.@progress for (i, κ) in enumerate(κs)                               # for each value of κ
        results[:, i+1] = (tstar * ω_infty(1) .+ (ts .- tstar) .* ω_infty(κ)) ./ ts     # get ω for each time point in ts     
    end

    # save the result in CSV-file
    writedlm(joinpath(@__DIR__, "$filename.csv"), results, ',')
end

@time ω_noneq_EW("omega_EW", [0.1, 0.25, 0.5, 1.0, 2.0, 4.0], 1.0, 19.0, 50) 
@time ω_noneq_EW("omega_EW_tstar=10", [0.1, 0.25, 0.5, 1.0, 2.0, 4.0], 10.0, 10.0, 50) 
@time ω_noneq_EW("omega_EW_tstar=18", [0.1, 0.25, 0.5, 1.0, 2.0, 4.0], 18.0, 2.0, 10) 