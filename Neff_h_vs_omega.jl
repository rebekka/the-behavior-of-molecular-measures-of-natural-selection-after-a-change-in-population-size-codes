### harmonic mean Nₑ vs ω ### 

# packages for simulations (reading in etc.)
using CSV
using DataFrames
using Query
using Distributions
using StatsBase
using StatsFuns
using BSON: @save, @load

# packages for integration
using FastGaussQuadrature
using HCubature
using LinearAlgebra

# other packages
using SpecialFunctions
using ProgressLogging
using DelimitedFiles


### ----------- ω ----------- ### 

### ----------- simulated ecdfs for different modes of selection ----------- ###

# I. get unconditional ecdf's for neutral selection
function load_cdfs_gamma(file)
    df = DataFrame(CSV.read(file))                      # read in the file and convert it to a dataframe

    cdfs = @from entry in df begin                      # for each entry in df:
        @group entry.t by entry.y, entry.gamma into g   # collect the fixation times for (y,γ) into g
        @select ecdf(g)                                 # get the ecdf for each (y,γ)
        @collect                                        # return the adjusted vector
    end
    cdfs
end

# read in the fixation files
const unconditional_cdfs_τ_neu = load_cdfs_gamma("unconditional_time_of_fixation_neutral.csv")

# II. simulation for negative selection
# simulation of times to fixation (unconditional on fixation, binomial sampling)
function get_time_to_fixation!(y, N, s, m)
    # y             initial frequency
    # N             population size 
    # s             selection coefficient (discrete)
    # m             total number of simulated paths

    init = Int(round(y * N))                # initial number of individuals carrying the derived allele
    τ = Array{Float64}(undef, m)            # array for saving time to fixations

    for i in 1:m                            # simulate m paths
        k = init                            # initial number of individuals carrying the derived allele
        j = 0                               # current generation
        while true
            if iszero(k)                    # if extinction
                τ[i] = Inf                  # time to fixation is infinity
                break                       # start a new path
            elseif k < N                    # sample according to binomial distribution
                p = k*(1+s)/(k*s+N)         # probability to 'sample' a derived allele
                k = rand(Binomial(N, p))    # sample number of derived alleles in next generation
                j += 1                      # update the generation
            else                            # if fixation
                τ[i] = j                    # save generation of fixation
                break                       # start a new path
            end
        end
    end
    τ ./= N                                 # scale τ to evolutionary time scale
end


### ----------- denominator Z_{t}^{0,κ} ----------- ###

# integrand of denominator
function f_neutral(y, idx, Δtoκ)
    # y             initial value
    # idx           index of ecdf (belonging to (y, γ = 0))
    # Δtoκ          evaluation time

    cdf = unconditional_cdfs_τ_neu[idx]     # get the corresponding cdf, P_{y}^{0,1}(τ₁≤(t-t*)/κ)
    cdf(Δtoκ) / y                           # integrand
end

# denominator Z_{t}^{0,κ}
function Z₀(κ, t, tstar, θ)
    # κ             population size factor
    # t             time
    # tstar         time of change in population size
    # θ             population scaled mutational input

    if t ≤ tstar
        θ * t
    else
        zs, ws = gausslegendre(100)                             # nodes and weights of Legendre polynomials
        ys = @. (1 + zs) / 2                                    # transform to [0, 1]

        Δtoκ = (t - tstar) / κ                                  # calculate once (t-t*)/κ
        fvals = [f_neutral(ys[i], i, Δtoκ) / 2 for i in 1:100]  # evaluate the integrand
        integral = dot(fvals, ws)                               # compute the integral
        θ * t + 2 * θ * (1 - κ) * integral                      # compute the whole expression
    end
end


### ----------- nominator Z_{t}^{γ,κ} ----------- ###

# pdf of the reflected Gamma-distribution
function reflected_gamma(γ, a, b)
    # γ             selection coefficient
    # a             shape parameter of gamma distribution
    # b             scale parameter of gamma distribution

    (-γ)^(a - 1) * exp(γ / b) / (b^a * gamma(a))
end

# definition of the fixation rate ω_{γ,κ} in equilibrium for fixed γ
function ω(γ, κ)
    # γ             selection coefficient
    # κ             population size factor

    if γ == 0
        ω = 1.0
    else
        γ2κ = 2 * γ * κ
        ω = γ2κ / (1 - exp(-γ2κ))
    end
end

# linear contribution
function linear_part(γ, κ, t, tstar, θ)
    # γ             selection coefficient
    # κ             population size factor
    # t             time
    # tstar         time of change in population size
    # θ             population scaled mutational input

    θ * (ω(γ, 1) * tstar + ω(γ, κ) * (t - tstar))
end

# nonlinear contribution 
function nonlinear_part(simulations, y, γ, κ, Δtoκ, θ; N = 1000, m = 10_000)
    # simulations   simulations for the cdf of the time to fixation
    # y             initial frequency
    # γ             selection coefficient
    # κ             population size factor
    # Δtoκ          (t-tstar) / κ --> evaluation time
    # θ             population scaled mutational input
    # N             population size (for simulations)
    # m             total number of simulated paths

    κ == 1 && return 0.0

    s = γ / N           # γ = Ns

    # get the cdf
    # round the values for s and y
    if s < 1/N    # if weakly selected, finer grid
        rs, ry = round(s; digits = 4) , round(y; digits = 2)
    else          # if strongly selected, sparser grid
        rs, ry = round(s; digits = 2) , round(y; digits = 2)
    end

    # check if simulation exists, if not, do the simulation
    cdf = get!(simulations, (rs, ry)) do
        # default value calculated here
        τ = get_time_to_fixation!(ry, N, rs, m)
        ecdf(τ)
    end

    # integrand
    γ2 = 2 * γ          # calculate once
    γ2κ = 2 * γ * κ     # calculate once
    result = θ * cdf(Δtoκ) * 2 / (y * (1 - y)) * (((exp(γ2) - exp(γ2 * y)) / (exp(γ2) - 1)) - (κ * (exp(γ2κ) - exp(γ2κ * y)) / (exp(γ2κ) - 1)))

    result
end

### ----------- ratio ω ----------- ###

# non-synonymous over synonymous fixation rate ratio ω(γ, κ, t, t*)
function ratio_ω(simulations, κ, t, tstar; θ = 1.0, a = 0.15, b = 2500 / 0.15)
    # simulations   simulations for the cdf of the time to fixation
    # κ             population size factor
    # t             time
    # tstar         time of change in population size
    # θ             population scaled mutational input
    # a             shape parameter of gamma distribution
    # b             scale parameter of gamma distribution

    if t <= tstar
        omega = hquadrature(γ -> (reflected_gamma(γ, a, b) * ω(γ, 1)), -1000, -eps()*10; rtol=1e-6)[1]   # θt cancels in the nominator and denominator
    else
        # nominator
        Δtoκ = (t - tstar) / κ
        nom = hcubature(x -> (reflected_gamma(x[2], a, b) * (linear_part(x[2], κ, t, tstar, θ) + nonlinear_part(simulations, x[1], x[2], κ, Δtoκ, θ))), [eps(), -1000], [1-eps(), -0.5eps() * 1e1]; rtol=1e-5)[1]   # x = (γ, y)

        # denominator
        denom = Z₀(κ, t, tstar, θ)

        # ratio
        omega = nom / denom
    end
    omega
end

#
function ω_infty(κ; θ = 1, a = 0.15, b = 2500 / 0.15)
    # κ             population size factor
    # θ             population scaled mutational input
    # a             shape parameter of gamma distribution
    # b             scale parameter of gamma distribution

    hquadrature(γ -> (reflected_gamma(γ, a, b) * ω(γ, κ)), -1000, -eps() * 10; rtol=1e-6)[1]
end

### ----------- harmonic mean Nₑ ----------- ###

function Nₑ(κ, t, tstar; N = 1000)
    # κ             population size factor
    # t             time
    # tstar         time of change in population size
    # N             ancestral / reference population size

    (κ * N * t) / (tstar * (κ - 1) + t)     # harmonic mean Nₑ
end

### ----------- harmonic mean Nₑ vs ω ----------- ### 

function Ne_vs_omega(filename, κs, tstar, ts; kwargs...)
    # filename      name of the file to be saved
    # κs            vector of different values of κ
    # tstar         time of change in population size
    # ts            time points

    @load "simulations.bson" simulations

    # time points for t > t*
    ts = tstar .+ ts
    lt = length(ts)

    # matrix for saving results
    results = Matrix{Float64}(undef, length(κs), 2 * lt)           

    # development after change in demography
    ProgressLogging.@progress for (i, κ) in enumerate(κs)   # for each value of κ
        # compute Nₑ      
        results[i, 1:lt]  = log.(Nₑ.(κ, ts, tstar))         # Nₑ 
        
        # compute ω  
        results[i, lt+1:end] = log.(ratio_ω.(Ref(simulations), κ, ts, tstar; kwargs...)) # ω            

        @show κ
        @show log(1000 * κ)
        @show log(ω_infty(κ))
    end

    # save the result in CSV-file
    writedlm(joinpath(@__DIR__, "$filename.csv"), results, ',')

    # save the dictionary "simulations" 
    @save "simulations.bson" simulations
end

@time Ne_vs_omega("Nh_vs_omega_ancient_loglog", [0.1, 0.25, 0.5, 2.0, 4.0], 1.0, [0, 1, 9, 19]) # for ancient change, used rtol= 1e-3 in line 197
@time Ne_vs_omega("Nh_vs_omega_recent_loglog", [0.1, 0.25, 0.5, 1.0, 2.0, 4.0], 18.0, [0, 2]) # for recent change, used rtol= 1e-5 in line 197


### ----------- harmonic mean Nₑ vs ω equilibrium ----------- ###

function Ne_vs_omega_equilibirum(; θ = 1.0, kwargs...)
    # θ             population scaled mutational input

    # matrix for saving results
    results_eq = Matrix{Float64}(undef, 50, 2)   

    # Ne_eq
    Ne_eq = range(100; stop = 4000, length = 50)   
    results_eq[:, 1] = log.(Ne_eq)

    # ω_eq
    results_eq[:, 2] = log.(ω_infty.(Ne_eq./1000))  
    
    # save the result in CSV-file
    writedlm(joinpath(@__DIR__, "Nh_vs_omega_eq_loglog.csv"), results_eq, ',')
end

@time Ne_vs_omega_equilibirum()