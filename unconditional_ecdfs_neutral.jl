# simulation of τ₁ for P_{y}^{0,1}(τ₁ ≤ t/κ) in ω

using Distributions
using FastGaussQuadrature
using ProgressLogging
using DataFrames
using CSV

# simulation of times to fixation (unconditional on fixation, binomial sampling)
function get_time_to_fixation!(y, N, m)
    # y     initial frequency
    # N     population size
    # m     total number of simulated paths

    s = 0                                   # selection coefficient
    init = y * N                            # initial number of individuals carrying the derived allele
    τ = Array{Float64}(undef, m)            # array for saving time to fixations

    for i in 1:m                            # simulate m paths
        k = init                            # initial number of individuals carrying the derived allele
        j = 0                               # current generation
        while true
            if iszero(k)                    # if extinction
                τ[i] = Inf                  # time to fixation is infinity
                break                       # start a new path
            elseif k < N                    # sample according to binomial distribution
                p = k*(1+s)/(k*s+N)         # probability to 'sample' a derived allele = k/N
                k = rand(Binomial(N, p))    # sample number of derived alleles in next generation
                j += 1                      # update the generation
            else                            # if fixation
                τ[i] = j                    # save generation of fixation
                break                       # start a new path
            end
        end
    end
    τ ./= N                                 # scale τ to evolutionary time scale
end


# returns fixation times for paths initialized at y and under selective pressure 0
function time_to_fixation(N, m)
    # N     population size
    # m     total number of fixations

    ys, wys = gausslegendre(100)                                                    # use nodes of Legendre polynomials as y-values (in [-1,1])
    @. ys = (1 + ys) / 2                                                            # transform to [0, 1]

    open("unconditional_time_of_fixation_neutral.csv", "w") do f                    # open a csv-file
        append = false                                                              # write the column titles
        # get fixation times for specific (y,0)
        ProgressLogging.@progress for y in ys                                       # for each y
                τ = get_time_to_fixation!(y, N, m)                                  # simulate paths
                CSV.write(f, DataFrame(y = y, gamma = 0, t = τ), append = append)   # add to the file
                append = true                                                       # add without new title
        end
    end
end

@time time_to_fixation(1000, 100_000)
# time:  880.853557 seconds (10.94 M allocations: 1.189 GiB, 0.02% gc time)
