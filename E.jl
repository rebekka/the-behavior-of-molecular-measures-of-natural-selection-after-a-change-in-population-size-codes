# simulation of E[ξ(1-ξ)] for negative selection coefficents 

using Distributions
using ProgressLogging
using DataFrames
using CSV

# simulation of Eyγ[ξ(1-ξ)] for a tuple (y, γ) at time points in [0, T]
function get_E!(y, N, s, m, T)
    # y    initial frequency
    # N    population size
    # s    selection coefficient
    # m    total number of simulated paths
    # T    end point of considered time interval

    init = y * N                                # initial number of individuals carrying the derived allele
    T *= N                                      # number of generations
    T += 1                                      # add "generation 0"
    simulations = Array{Float64}(undef, m, T)   # matrix for saving the states
    E = Array{Float64}(undef, T)                # array for saving target quantity E[ξ(1-ξ)]
    simulations[:,1] .= init                    # status in generation 1
    E[1] = y * (1 - y)                          # initially E[ξ(1-ξ)] = y*(1-y)

    for i in 1:m                                # simulate m paths
        k = init                                # initial number of individuals carrying the derived allele
        j = 2                                   # current generation
        while j <= T
            if iszero(k)                        # in case of extinction
                simulations[i,j:end] .= 0       # set everything to zero
                break                           # go to next simulation
            elseif k < N                        # sample according to binomial distribution
                p = k*(1+s)/(k*s+N)             # probability to 'sample' a derived allele
                k = rand(Binomial(N, p))        # sample number of derived alleles in next generation
                simulations[i, j] = k           # save status in generation j
                j += 1                          # update the generation
            else                                # in case of fixation
                simulations[i,j:end] .= N       # set everything to N
                break                           # go to next simulation
            end
        end
    end

    simulations ./= N                           # scale to [0,1]

    for t in 2:T
        E[t] = mean(simulations[:,t]) - mean(simulations[:,t].^2)   # calculate E[ξ(1-ξ)] for each generation
    end
    E
end

# returns E[ξ(1-ξ)] for paths initialized at y and under selective pressure s
function Expected_Value(N, m, T)
    # N     population size
    # m     total number of simulated paths
    # T     end point of considered time interval

    # define (y,γ)-tuples
    nodes = Iterators.product(0.01:0.01:0.99, [-100, -10, -1, -0.9, -0.8, -0.7, -0.6, -0.5, -0.4, -0.3, -0.2, -0.1])

    open(joinpath(@__DIR__, "E.csv"), "w") do f                                         # open a csv-file
        append = false                                                                  # write the column titles
        # get E for specific (y,γ)
        ProgressLogging.@progress for (y, γ) in nodes                                   # for each tuple
                E = get_E!(y, N, γ/N, m, T)                                             # simulate paths
                df = DataFrame(y = y, gamma = γ, t = (1:length(E))./N, E = E)           # write into DataFrame
                df_filtered = filter(x -> x.t in range(0, step = 0.01, stop = 200), df) # filter only certain time steps
                CSV.write(f, df_filtered, append = append)                              # add to the file
                append = true                                                           # add without new title
        end
    end
end

@time Expected_Value(1000, 1000, 200)

