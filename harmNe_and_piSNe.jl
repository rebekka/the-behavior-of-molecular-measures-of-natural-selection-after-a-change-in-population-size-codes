### plot proxies of Nₑ ###

using ProgressLogging
using DelimitedFiles

### ----------- Nₑ based on nucleotide variation ----------- ###

function Nₑπ(κ, t, tstar, N)
    # κ             population size factor
    # t             time
    # tstar         time of change in population size
    # N             ancestral / reference population size

    N * (κ + (1 - κ) * exp(- max(0, t - tstar) / κ))    # corresponds to πₛ/(2μ)
end

### ----------- harmonic mean Nₑ ----------- ###

function Nₑharmonic(κ, t, tstar, N)
    # κ             population size factor
    # t             time
    # tstar         time of change in population size
    # N             ancestral / reference population size

    (κ * N * t) / (tstar * (κ - 1) + t)     # harmonic Nₑ
end

### ----------- Nₑπ vs Nₑharmonic ----------- ###

function Nₑπ_vs_Nₑharmonic(filename, κs, tstar, T, lt; kwargs...)
    ts = range(tstar + eps(); stop = tstar + T, length = lt)
    return Nₑπ_vs_Nₑharmonic(filename, κs, tstar, ts; kwargs...)
end

function Nₑπ_vs_Nₑharmonic(filename, κs, tstar, ts; N=1_000, kwargs...)
    # figname       name of the figure to be saved
    # κs            vector of different values of κ
    # tstar         time of change in population size
    # T             number of time units
    # lt            length of time vector t
    # N             ancestral / reference population size

    # matrix for saving results
    lt = length(ts)
    results_Nₑharmonic = Matrix{Float64}(undef, lt, length(κs) + 1)        
    results_Nₑπ = Matrix{Float64}(undef, lt, length(κs) + 1)  
    
    # time points for t > t*
    results_Nₑharmonic[:, 1] = ts
    results_Nₑπ[:, 1] = ts      

    # development after change in demography
    ProgressLogging.@progress for (i, κ) in enumerate(κs), (j, t) in enumerate(ts)
        # compute Nₑ      
        results_Nₑharmonic[j, i+1]  = Nₑharmonic(κ, t, tstar, N) # harmonic mean Nₑ
        
        # compute Nₑπ  
        results_Nₑπ[j, i+1] =  Nₑπ(κ, t, tstar, N)               # Nₑ based on nucleotide variation
    end

    # save the result in CSV-file (time, equilibirum, κs)
    writedlm(joinpath(@__DIR__, "Ne_harmonic_$(filename).csv"), results_Nₑharmonic, ',')
    writedlm(joinpath(@__DIR__, "Ne_pi_$(filename).csv"), results_Nₑπ, ',')
end

@time Nₑπ_vs_Nₑharmonic("ancient_change_test", [0.1, 0.25, 0.5, 1.0, 2.0, 4.0], 1.0, 19.0, 200)
@time Nₑπ_vs_Nₑharmonic("recent_change", [0.1, 0.25, 0.5, 1.0, 2.0, 4.0], 18.0, 2.0, 200)
