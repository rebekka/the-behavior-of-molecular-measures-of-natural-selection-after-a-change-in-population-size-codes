### πₙ/πₛ ###

# packages for simulations (reading in etc.)
using CSV
using TableOperations
using Tables
using Distributions
using StatsBase
using StatsFuns

# packages for integration
using FastGaussQuadrature
using LinearAlgebra
using HCubature
using StaticArrays

# other packages
using SpecialFunctions
using ProgressLogging
using DelimitedFiles

### ------------------- πₛ ------------------- ###

function πₛ(κ, t, tstar, θ)
    # κ             population size factor
    # t             time
    # tstar         time of change in population size
    # θ             population scaled mutational input

    2 * θ * (κ + (1 - κ) * exp(- max(0, t - tstar) / κ))
end

### ------------------- πₙ ------------------- ###

# read in simulations for E[ξ(1-ξ)]
const simulations = CSV.File("E.csv")   

# filter only relevant simulations
function filter_simulations(κ, tstar, T, lt)
    # κ             population size factor
    # tstar         time of change in population size
    # T             end point of [0, T]
    # lt            length of time vector t

    ts = tstar .+ range(0; stop = T, length = lt)   
    Δt = (ts .- tstar) ./ κ
    rΔt = round.(Δt; digits = 2)
   
    # filter simulations only at timepoints used
    return TableOperations.filter(x -> x.t in rΔt, simulations) |> Tables.rowtable
end

function load_filtered_simulations(κ, tstar, T, lt)
    # κ             population size factor
    # tstar         time of change in population size
    # T             end point of [0, T]
    # lt            length of time vector t

    filename = joinpath(@__DIR__,"csv-files","filtered_simulations_kappa=$(κ)_tstar=$(tstar)_T=$(T)_lt=$(lt).csv")

    # if file already exists
    if isfile(filename)
        return CSV.File(filename)
    end

    # otherwise, filter simulations
    data = filter_simulations(κ, tstar, T, lt)
    CSV.write(filename, data)
    return CSV.File(filename)
end

# function for getting E[ξ(1-ξ)] for triplet (y,γ,t)
function load_E(simulations, y, γ, t)
    # simulations   simulations of E[ξ(1-ξ)]
    # y             initial frequency
    # γ             selection coefficient
    # t             time

    for row in simulations
        if row.y == y && row.gamma == γ && row.t == t
            return row.E
        end
    end

    @show y, γ, t
    nothing
end

# help function for finding closest simulated γ
function findclosest(γ_vals, γ) 
    # γ_vals        filtered γ values 
    # γ             selection coefficient

    idx = findmin(abs.(γ_vals .- γ))[2]
    γ_vals[idx]
end

# --------------------------------------------------------------------------#
# pdf of the reflected Gamma-distribution
function reflected_gamma(γ, a, b)
    # γ             selection coefficient
    # a             shape parameter of gamma distribution
    # b             scale parameter of gamma distribution

    (-γ)^(a - 1) * exp(γ / b) / (b^a * gamma(a))
end

# --------------------------------------------------------------------------#
# linear contribution 
function linear_part(y, γ, κ)
    # y             initial frequency
    # γ             selection coefficient
    # κ             population size factor

    γ2κ = 2 * γ * κ     # calculate once
    κ * (exp(γ2κ) - exp(γ2κ * y)) / (exp(γ2κ) - 1)
end

# --------------------------------------------------------------------------#
# nonlinear contribution 

# get E[ξ(1-ξ)] for triplet (y,γ,Δt)
function get_E(simulations, y, γ, Δt)
    # simulations   simulations of E[ξ(1-ξ)]
    # y             initial value
    # γ             selection coefficient
    # Δt            time t-t*

    rΔt = round(Δt; digits = 2)
    if rΔt ≤ 0
        return y * (1 - y)                          # if t=t*: E[ξ(1-ξ)] = y * (1 - y)  
    end

    # round the values for y and γ
    ry = round(y; digits = 2)           
    if ry == 0.0 || ry == 1.0
       return 0.0                                   # if fixed or extinct: E[ξ(1-ξ)] = 0
    end

    γ_vals = [-100, -10, -1, -0.9, -0.8, -0.7, -0.6, -0.5, -0.4, -0.3, -0.2, -0.1, 0.0]
    rγ = findclosest(γ_vals, γ)
    if rγ == 0.0
        return exp(-rΔt) * y * (1 - y)              # if γ=0: use Ito's formula -> E[ξ(1-ξ)] = exp(-rΔt) * y * (1 - y)
    else
        return load_E(simulations, ry, rγ, rΔt)     # otherwise: load simulations
    end
end

function nonlinear_part(simulations, y, γ, Δt, κ)
    # simulations   simulations of E[ξ(1-ξ)]
    # y             initial value
    # γ             selection coefficient
    # Δt            time t-t*
    # κ             population size factor

    γ2 = 2 * γ          # calculate once
    γ2κ = 2 * γ * κ     # calculate once
    
    return get_E(simulations, y, γ, Δt) / (y * (1- y)) * (((exp(γ2) - exp(γ2 * y)) / (exp(γ2) - 1)) - (κ * (exp(γ2κ) - exp(γ2κ * y)) / (exp(γ2κ) - 1)))
end

# --------------------------------------------------------------------------#
# numerical integration

function numerical_hcubature(simulations, κ, t, tstar; θ = 1.0, a = 0.15, b = 2500 / 0.15, kwargs...)
    # simulations   simulations of E[ξ(1-ξ)]
    # κ             population size factor
    # t             time
    # tstar         time of change in population size
    # θ             population scaled mutational input
    # a             shape parameter of gamma distribution
    # b             scale parameter of gamma distribution
    
    # integration over γ and y
    hcubature(@SVector([-1000, 0]), @SVector([-1e-5, 1]); rtol = 1e-3) do x
        γ = x[1]
        y = x[2]
        Δt = (t - tstar) / κ

        return 4 * θ * reflected_gamma(γ, a, b) * (linear_part(y, γ, κ) + nonlinear_part(simulations, y, γ, Δt, κ))
    end
end

### ------------------- πₙ/πₛ ------------------- ###

function piNpiS(filename, κs, tstar, T, lt; θ = 1.0, a = 0.15, b = 2500 / 0.15, kwargs...)
    # filename      name of the file to be saved
    # κs            vector of different values of κ
    # tstar         time of change in population size
    # T             number of time units
    # lt            length of time vector t
    # θ             population scaled mutational input
    # a             shape parameter of gamma distribution
    # b             scale parameter of gamma distribution
    
    # # linear development before change in demography, t ≦ t*
    # πS = 2 * θ 
    # simulations_filtered = load_filtered_simulations(1.0, tstar, T, lt)
    # πN = [numerical_hcubature(simulations_filtered, 1.0, t, tstar)[1] for t in (zero(tstar), tstar)]
    # -->  (πN /πS)

    # time points for t > t*
    ts = tstar .+ range(0; stop = T, length = lt)      

    # development after change in demography, t > t*
    # matrix for saving results
    results = Matrix{Float64}(undef, lt, length(κs) + 1)        
    results[:, 1] = ts

    ProgressLogging.@progress for (i, κ) in enumerate(κs)                               # for each value of κ
        πS = [πₛ.(κ, t, tstar, θ) for t in ts]                                          # πS
        simulations_filtered = load_filtered_simulations(κ, tstar, T, lt)
        πN = [numerical_hcubature(simulations_filtered, κ, t, tstar)[1] for t in ts]    # πN
        results[:, i+1] = πN ./ πS
    end

    # save the result in CSV-file
    writedlm(joinpath(@__DIR__, "$filename.csv"), results, ',')
end

@time piNpiS("piNpiS", [0.1, 0.25, 0.5, 1.0, 2.0, 4.0], 1.0, 19.0, 50)
